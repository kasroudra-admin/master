import React, { Component } from "react";
import "./hobbiesCard.css";
import { hobbies } from "../../portfolio";
import { Fade } from "react-reveal";

class HobbiesCard extends Component {
  render() {
  	const theme = this.props.theme;
    return (
      <div
        className="hobbies-card"
        style={{ border: `1px solid ${hobbies["color"]}` }}
      >
       <div className="hobbies-card-body-div">
          {hobbies.map((hobbiesdata) => {
            return (
              <div>
                <Fade left duration={2500}>
                  <h3
                    className="hobbies-card-title"
                    style={{ color: theme.text }}
                  >
                    {hobbiesdata.title}
                  </h3>
                  <p
                    className="hobbies-card-description"
                    style={{ color: theme.secondaryText }}
                  >
                    {hobbiesdata.description}
                  </p>
                </Fade>
                <Fade right duration={2500}>
                  <img
                    className="hobbies-image"
                    src={require(`../../assests/images/${hobbiesdata["image_path"]}`)}
                    alt={hobbiesdata.title}
                  />
                </Fade>
              </div>
            );
            })}
      </div>
      </div>
    );
  }
}

export default HobbiesCard;
