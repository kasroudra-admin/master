import React, { Component } from "react";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import TopButton from "../../components/topButton/TopButton";
import HobbiesCard from "../../components/hobbiesCard/hobbiesCard.js";
import "./Hobbies.css";
import { Fade } from "react-reveal";

class Hobbies extends Component {
  render() {
    const theme = this.props.theme;
    return (
      <div className="hobbies-main">
        <Header theme={theme} />
        <div className="basic-hobbies">
          <Fade bottom duration={2000} distance="40px">
            <div className="hobbies-heading-div">
              <div className="hobbies-heading-text-div">
                <h1
                  className="hobbies-heading-text"
                  style={{ color: theme.text }}
                >
                  Hobbies
                </h1>
              </div>
            </div>
          </Fade>
        </div>
        <HobbiesCard theme={theme} />
        <Footer theme={this.props.theme} onToggle={this.props.onToggle} />
        <TopButton theme={this.props.theme} />
      </div>
    );
  }
}

export default Hobbies;